package com.example.demo.socket;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.lang.UUID;

import javax.websocket.EndpointConfig;
import javax.websocket.Session;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 公共逻辑
 *
 * @author yzd
 */
public abstract class BaseMediaController extends BaseController {

	@Override
	public void onOpen(Session session, EndpointConfig config) {
		// 设置用户信息
		Map<String, List<String>> map = session.getRequestParameterMap();
		setSession(session);
		List<String> uids = map.get("uid");
		// 允许匿名
		if (CollectionUtil.isEmpty(uids)) {
			ArrayList<String> objects = new ArrayList<>();
			objects.add(UUID.randomUUID().toString().replace("-", ""));
			uids = new ArrayList<>(objects);
		}
		setUserName(uids.get(0));
		super.onOpen(session, config);
	}

}
