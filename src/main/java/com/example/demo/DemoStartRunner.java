package com.example.demo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * 程序启动 任务
 *
 * @author : YZD
 * @date : 2021-7-29 09:14
 */
@Slf4j
@Component
public class DemoStartRunner implements CommandLineRunner {
    @Override
    public void run(String... args) throws Exception {
        log.info("程序已启动");
    }
}
